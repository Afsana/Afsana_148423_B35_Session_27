<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <!--
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
    -->
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
</head>

<h2>Book Title</h2>

<?php

require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use App\Message\Message;

$objBookTitle = new BookTitle();
$allData = $objBookTitle->index("obj");

$serial = 1;


echo "<table border='2px'>";
echo "<th style='text-align:center'> Serial </th><th style='text-align:center'> ID </th><th style='text-align:center'> Book Title </th><th style='text-align:center'> Author Name </th><th style='text-align:center'> Action </th>";
foreach($allData as $oneData){
    //echo $oneData->id." - ".$oneData->book_title." - ".$oneData->author_name."<br>";
    echo "<tr style height ='50px'>";
    echo "<td> $serial </td>";
    echo "<td> $oneData->id </td>";
    echo "<td> $oneData->book_title </td>";
    echo "<td> $oneData->author_name </td>";

    echo "
        <td>
            <a href='view.php?id=$oneData->id'><button class='btn btn-success'>View</button></a>
            <a href='edit.php?id=$oneData->id'><button class='btn btn-primary'>Edit</button></a>
            <a href='trash.php?id=$oneData->id'><button class='btn btn-info'>Trash</button></a>
            <a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a>
        </td>
        ";
    echo"</tr>";
    $serial++;

}//end of foreach loop

echo "</table>";
?>

