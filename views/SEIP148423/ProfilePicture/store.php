<?php
include_once('../../../vendor/autoload.php');
use App\ProfilePicture\ProfilePicture;

/*
if((isset($_FILES['image'])) && (!empty($_FILES['image']['name']))){
    $imageName= time(). $_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];

    move_uploaded_file($temporary_location,'../../Resource/Images/'.$imageName);


    $_POST['image']=$imageName;

}
*/


$objProfilePicture = new ProfilePicture();

    $imageName = time() . $_FILES['profile_picture']['name'];
    $tmpLocation = $_FILES['profile_picture']['tmp_name'];
    $fileLocation = "../../../resource/assets/img/backgrounds/" . $imageName;
    move_uploaded_file($tmpLocation, $fileLocation);
    $_POST['filePath'] = $fileLocation;

$objProfilePicture->setData($_POST);

$objProfilePicture->store();
