<?php

namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class ProfilePicture extends DB{

    public $id="";
    public $name="";
    public $filePath="";

    public function __construct()
    {
        parent::__construct();
    }

/*
    public function index(){
        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->profile_picture."<br>";
    }
*/

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }

        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('filePath',$data)){
            $this->filePath=$data['filePath'];
        }

    }

    public function store(){
        $arrData=array($this->name, $this->filePath);
        $sql="INSERT INTO  profile_picture(name,image) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");

        Utility::redirect('create.php'); // redirect korte hobe create.php te tai utility.php use korechi //

    }//end of store method

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from profile_picture');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from profile_picture where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of index();
}


//$objBooktitle = new ProfilePicture();

?>
